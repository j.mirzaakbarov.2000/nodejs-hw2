const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/userModel');
require('dotenv').config({ path: './../config.env' });

exports.registerUser = async (req, res) => {
  try {
    console.log(req.body);
    const { username, password } = req.body;
    if (!username || !password) {
      return res.status(400).json({
        message: 'Invalid username or password',
      });
    }
    await User.create({
      username: username,
      password: await bcrypt.hash(password, 10),
    });
    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.loginUser = async (req, res) => {
  try {
    const { username, password } = req.body;

    const user = await User.findOne({ username: username });

    if (!user) {
      return res.status(400).json({
        message: 'Wrong username or password',
      });
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({
        message: 'Wrong username or password',
      });
    }

    const token = jwt.sign(
      {
        _id: user._id,
      },
      process.env.JWTSECRET
    );

    return res.status(200).json({
      message: 'Success',
      jwt_token: token,
    });
  } catch (err) {
    return res.status(500).json({
      message: err.message,
    });
  }
};
