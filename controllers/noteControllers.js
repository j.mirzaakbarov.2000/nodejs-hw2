const { ObjectId } = require('mongoose').Types;
const Note = require('../models/noteModel');

exports.getAllNotes = async (req, res) => {
  try {
    const notesCount = await Note.countDocuments({ userId: req.user._id });
    const { offset, limit } = req.query;

    const notes = await Note.find(
      { userId: req.user._id },
      {},
      { skip: +offset || 0, limit: +limit || 0 }
    ).select('-__v');

    res.status(200).json({
      offset: +offset || 0,
      limit: +limit || 0,
      count: notesCount,
      notes,
    });
  } catch (err) {
    return res.status(400).json({
      message: err.message,
    });
  }
};

exports.createNote = async (req, res) => {
  try {
    if (!req.body.text) {
      return res.status(400).json({
        message: 'Please enter note text',
      });
    }

    await Note.create({
      text: req.body.text,
      userId: req.user._id,
    });

    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(500).json({
      message: err.message,
    });
  }
};

exports.getNote = async (req, res) => {
  try {
    if (!ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message: 'Enter valid ID',
      });
    }
    const note = await Note.findOne({
      id: req.params.id,
      userId: req.user._id,
    }).select('-__v');

    if (!note) {
      return res.status(400).json({
        message: 'No note with such ID',
      });
    }

    res.status(200).json({
      note,
    });
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.updateNote = async (req, res) => {
  try {
    if (!ObjectId.isValid(req.params.id) || !req.body.text) {
      return res.status(400).json({
        message: 'Enter valid ID and note text',
      });
    }
    const note = await Note.findOneAndUpdate(
      {
        id: req.params.id,
        userId: req.user._id,
      },
      { text: req.body.text },
      { runValidators: true }
    );

    if (!note) {
      return res.status(400).json({
        message: 'No note with such ID',
      });
    }
    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.toggleNote = async (req, res) => {
  try {
    if (!ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message: 'Enter valid ID',
      });
    }

    const note = await Note.findOne({
      id: req.params.id,
      userId: req.user._id,
    });

    note.completed = !note.completed;
    await note.save();

    if (!note) {
      return res.status(400).json({
        message: 'No note with such id',
      });
    }
    res.status(200).json({
      message: 'Success',
      note,
    });
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.deleteNote = async (req, res) => {
  try {
    if (!ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message: 'Enter valid ID',
      });
    }

    const note = await Note.findOneAndRemove({
      _id: req.params.id,
      userId: req.user._id,
    });

    if (!note) {
      return res.status(400).json({
        message: 'No note with such ID',
      });
    }
    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(500).json({
      message: err.message,
    });
  }
};

exports.invalidDeleteNote = (req, res) => {
  res.status(400).json({
    message: 'Specify note id query params',
  });
};
