const bcrypt = require('bcryptjs');
const Note = require('../models/noteModel');
const User = require('../models/userModel');

exports.getUserInfo = (req, res) => {
  res.status(200).json({
    user: req.user,
  });
};

exports.changeUserPassword = async (req, res) => {
  try {
    if (!req.body.oldPassword || !req.body.newPassword) {
      return res.status(400).json({
        message: 'Enter old and new passwords',
      });
    }
    const { oldPassword, newPassword } = req.body;
    const { _id } = req.user;
    console.log(_id, oldPassword, newPassword);
    const user = await User.findById(_id);
    const match = await bcrypt.compare(oldPassword, user.password);

    if (!match) {
      return res.status(400).json({
        message: 'Password does not match',
      });
    }

    const password = await bcrypt.hash(newPassword, 10);
    user.password = password;
    await user.save();

    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(500).json({
      message: err.message,
    });
  }
};

exports.deleteUser = async (req, res) => {
  const { _id } = req.user;

  await User.findByIdAndRemove(_id);
  await Note.deleteMany({ userId: _id });

  res.status(200).json({
    message: 'Success',
  });
};
