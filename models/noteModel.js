const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  text: {
    type: String,
    required: [true, 'A note must have a text!'],
    trim: true,
  },
  userId: {
    type: String,
    required: [true, 'A note must have an owner'],
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
