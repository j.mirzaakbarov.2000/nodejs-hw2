const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
require('dotenv').config({ path: './../config.env' });

const authMiddleware = async (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).json({
      message: 'Please provide "authorization" header',
    });
  }

  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({ message: 'Please provide a token' });
  }

  const { _id } = jwt.verify(token, process.env.JWTSECRET);

  const user = await User.findById(_id)
    .select('username createdDate _id')
    .lean();

  if (!user) {
    return res.status(400).json({
      message: 'User does not exist',
    });
  }
  req.user = user;
  next();
};

module.exports = authMiddleware;
