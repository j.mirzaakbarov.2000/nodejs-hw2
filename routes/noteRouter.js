const express = require('express');
const noteController = require('../controllers/noteControllers');
const authMiddleware = require('../middleware/authMiddleware');

const router = express.Router();

router.use(authMiddleware);
router
  .route('/')
  .get(noteController.getAllNotes)
  .post(noteController.createNote)
  .delete(noteController.invalidDeleteNote);

router
  .route('/:id')
  .get(noteController.getNote)
  .put(noteController.updateNote)
  .patch(noteController.toggleNote)
  .delete(noteController.deleteNote);

module.exports = router;
