const express = require('express');
const userController = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');

const router = express.Router();

router.use(authMiddleware);
router
  .route('/me')
  .get(userController.getUserInfo)
  .patch(userController.changeUserPassword)
  .delete(userController.deleteUser);

module.exports = router;
