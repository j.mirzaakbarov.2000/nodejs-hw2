const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const noteRouter = require('./routes/noteRouter');
const userRouter = require('./routes/userRouter');
const authRouter = require('./routes/authRouter');
const customErrorHandler = require('./helpers/apiHelper');

const app = express();

// MIDDLEWARES
app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
app.use('/api/auth', authRouter);
app.use('/api/notes', noteRouter);
app.use('/api/users', userRouter);
app.use(express.static('public'));
app.use(customErrorHandler);

module.exports = app;
